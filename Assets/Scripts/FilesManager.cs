using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FilesManager : MonoBehaviour
{
    public static FilesManager Instance;
    [SerializeField] private Files filesInventory;
    [SerializeField] private GameObject prefabSprite;
    [SerializeField] private GameObject prefabText;

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        DisplayFIles();
    }

    public void DisplayFIles()
    {
        int idChild = 0;
        for(int i = 0; i < transform.childCount; i++)
        {
            if (i < filesInventory.files.Count)
            {
                GameObject item = Instantiate(prefabSprite, transform.GetChild(idChild));
                GameObject text = Instantiate(prefabText, transform.GetChild(idChild));
                item.GetComponent<Image>().sprite = filesInventory.files[i].icon;
                Image itemImage = item.GetComponent<Image>();
                DraggableFile draggableFile = transform.GetChild(idChild).GetComponent<DraggableFile>();
                //draggableFile.LinkItem(filesInventory.files[i]);
                text.GetComponent<Text>().text = filesInventory.files[i].nameItem;
                idChild++;
            }
        }
    }
}
