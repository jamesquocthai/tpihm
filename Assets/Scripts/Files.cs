using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Files : ScriptableObject
{
    public List<ScriptableFile> files = new List<ScriptableFile>();
}
