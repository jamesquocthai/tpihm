using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class DraggableFile : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    // private Vector2 originalPosition;
    // public List<RectTransform> targets; Assign this list in the inspector
    // public float thresholdDistance = 100f; Distance threshold for pop action

    //[SerializeField] private TextMeshProUGUI nameText;
    //[SerializeField] private Image icon;
    //[SerializeField] private Sprite emptyIcon;

    //private Dictionary<RectTransform, Vector2> targetOriginalPositions;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    private ScriptableFile linkedItem;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Files filesInventory;
    [SerializeField] private int i;
    [SerializeField] private GameObject prefabSprite;
    [SerializeField] private GameObject prefabText;

    private void Start()
    {
        
    }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        LinkItem();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("OnDrag");
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    public void LinkItem()
    {
        GameObject item = Instantiate(prefabSprite, transform);
        GameObject text = Instantiate(prefabText, transform);
        text.GetComponent<Text>().text = filesInventory.files[i].nameItem;
        item.GetComponent<Image>().sprite = filesInventory.files[i].icon;
        Image itemImage = item.GetComponent<Image>();
        linkedItem = filesInventory.files[i];
    }

    private void OnButtonClick()
    {
        if (linkedItem != null)
        {
        }
    }

/*
    private void Awake()
    {
        // Store the original positions of the targets
        targetOriginalPositions = new Dictionary<RectTransform, Vector2>();
        foreach (var target in targets)
        {
            targetOriginalPositions[target] = target.anchoredPosition;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        // Save the original position of the draggable item
        originalPosition = GetComponent<RectTransform>().anchoredPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        // Move the draggable item
        GetComponent<RectTransform>().anchoredPosition += eventData.delta;

        // Check each target's distance to the draggable item
        foreach (var target in targets)
        {
            float distance = Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, target.anchoredPosition);
            if (distance > thresholdDistance)
            {
                // Calculate the direction and "pop" the target towards the draggable item
                Vector2 direction = (GetComponent<RectTransform>().anchoredPosition - targetOriginalPositions[target]).normalized;
                target.anchoredPosition = targetOriginalPositions[target] + direction * thresholdDistance;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Return the draggable item to its original position
        GetComponent<RectTransform>().anchoredPosition = originalPosition;

        // Return all targets to their original positions
        foreach (var target in targets)
        {
            target.anchoredPosition = targetOriginalPositions[target];
        }
    }*/
}
