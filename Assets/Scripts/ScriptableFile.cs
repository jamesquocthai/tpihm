using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ScriptableFile : ScriptableObject
{
    public string nameItem;
    public Sprite icon;

    public bool Equals(ScriptableFile i)
    {
        return i.nameItem == this.nameItem;
    }
}
